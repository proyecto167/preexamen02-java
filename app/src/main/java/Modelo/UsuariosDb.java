package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.preexamen02_java.Usuario;

import java.util.ArrayList;

public class UsuariosDb implements Persistencia, Proyeccion{

    private Context context;
    private UsuarioDbHelper helper;
    private SQLiteDatabase db;

    public UsuariosDb(Context context, UsuarioDbHelper helper){
        this.context=context;
        this.helper=helper;
    }

    public UsuariosDb(Context context){
        this.context=context;
        this.helper=new UsuarioDbHelper(this.context);
    }


    @Override
    public void openDataBase() {
        db=helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    public long insertUsuario(Usuario usuario) {

        ContentValues values=new ContentValues();

        values.put(DefineTable.Usuarios.COLUMN_NAME_NOMBREUSUARIO, usuario.getTextNombreUsuario());
        values.put(DefineTable.Usuarios.COLUMN_NAME_CORREO, usuario.getTextCorreo());
        values.put(DefineTable.Usuarios.COLUMN_NAME_CONTRASENA, usuario.getTextContrasena());

        this.openDataBase();
        long num = db.insert(DefineTable.Usuarios.TABLE_NAME, null, values);
        this.closeDataBase();
        Log.d("agregar","insertUsuario: "+num);

        return num;
    }

    @Override
    public long updateUsuario(Usuario usuario) {
        ContentValues values=new ContentValues();

        values.put(DefineTable.Usuarios.COLUMN_NAME_NOMBREUSUARIO, usuario.getTextNombreUsuario());
        values.put(DefineTable.Usuarios.COLUMN_NAME_CORREO, usuario.getTextCorreo());
        values.put(DefineTable.Usuarios.COLUMN_NAME_CONTRASENA, usuario.getTextContrasena());

        this.openDataBase();
        long num = db.update(
                DefineTable.Usuarios.TABLE_NAME,
                values,
                DefineTable.Usuarios.COLUMN_NAME_ID+"="+usuario.getId(),
                null);
        this.closeDataBase();
        Log.d("agregar","insertUsuario: "+num);

        return num;
    }

    @Override
    public void deleteUsuarios(int id) {
        this.openDataBase();
        db.delete(
                DefineTable.Usuarios.TABLE_NAME,
                DefineTable.Usuarios.COLUMN_NAME_ID+"=?",
                new String[] {String.valueOf(id)});
        this.closeDataBase();
    }


    @Override
    public Usuario getUsuario(String correo) {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTable.Usuarios.TABLE_NAME,
                DefineTable.Usuarios.REGISTRO,
                DefineTable.Usuarios.COLUMN_NAME_CORREO + " = ?",
                new String[]{correo},
                null, null, null);

        if (cursor.moveToFirst()) {
            Usuario usuario = readUsuario(cursor);
            cursor.close();
            return usuario;
        } else {
            cursor.close();
            return null; // El usuario no se encontró en la base de datos
        }
    }

    @Override
    public ArrayList<Usuario> allUsuarios() {
        db=helper.getWritableDatabase();

        Cursor cursor=db.query(
                DefineTable.Usuarios.TABLE_NAME,
                DefineTable.Usuarios.REGISTRO,
                null, null, null, null, null);
        ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            Usuario usuario=readUsuario(cursor);
            usuarios.add(usuario);
            cursor.moveToNext();
        }

        cursor.close();
        return usuarios;
    }

    @Override
    public Usuario readUsuario(Cursor cursor) {
        Usuario usuario=new Usuario();

        usuario.setId(cursor.getInt(0));
        usuario.setTextNombreUsuario(cursor.getString(1));
        usuario.setTextCorreo(cursor.getString(2));
        usuario.setTextContrasena(cursor.getString(3));

        return usuario;
    }
}
