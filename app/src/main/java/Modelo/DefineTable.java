package Modelo;

public class DefineTable {

    public DefineTable(){}

    public static abstract class Usuarios{
        public static final String TABLE_NAME="usuarios";
        public static final String COLUMN_NAME_ID="id";
        public static final String COLUMN_NAME_NOMBREUSUARIO="nombreUsuario";
        public static final String COLUMN_NAME_CORREO="correo";
        public static final String COLUMN_NAME_CONTRASENA="contrasena";

        public static String[] REGISTRO = new String[]{
                Usuarios.COLUMN_NAME_ID,
                Usuarios.COLUMN_NAME_NOMBREUSUARIO,
                Usuarios.COLUMN_NAME_CORREO,
                Usuarios.COLUMN_NAME_CONTRASENA
        };
    }
}
