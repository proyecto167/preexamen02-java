package Modelo;

import com.example.preexamen02_java.Usuario;

public interface Persistencia {

    public void openDataBase();
    public void closeDataBase();
    public long insertUsuario(Usuario usuario);
    public long updateUsuario(Usuario usuario);
    public void deleteUsuarios(int id);

}
