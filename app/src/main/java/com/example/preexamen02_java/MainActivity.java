package com.example.preexamen02_java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import Modelo.UsuariosDb;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private AdapterUsuario adapter;
    private List<Usuario> listaUsuarios;
    private UsuariosDb usuariosDb;
    private Button btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usuariosDb = new UsuariosDb(this);

        // Obtener la referencia de la ListView
        listView = findViewById(R.id.lstUsuarios);

        btnRegresar = findViewById(R.id.btnRegresar);

        // Crear la lista de usuarios
        //listaUsuarios = Usuario.llenarUsuarios();
        ArrayList<Usuario> usuarios = usuariosDb.allUsuarios();
        adapter = new AdapterUsuario(this, usuarios);
        listView.setAdapter(adapter);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Regresar a la pantalla de inicio de sesión
                Intent intent = new Intent(MainActivity.this, loginActivity.class);
                startActivity(intent);
                finish(); // Cierra la actividad actual (MainActivity)
            }
        });

        // Crear el adaptador y establecerlo en la ListView
        //adapter = new AdapterUsuario(this, listaUsuarios);
        //listView.setAdapter(adapter);
    }
}