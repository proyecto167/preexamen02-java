package com.example.preexamen02_java;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterUsuario extends BaseAdapter {
    private List<Usuario> datos;
    private Context context;

    public AdapterUsuario(Context context, List<Usuario> datos) {
        this.context = context;
        this.datos = datos;
    }

    @Override
    public int getCount() {
        return datos.size();
    }

    @Override
    public Object getItem(int position) {
        return datos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            // Inflar el diseño personalizado del elemento de la lista
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.usuarios_item, parent, false);

            // Crear un ViewHolder para mantener las referencias a las vistas
            viewHolder = new ViewHolder();
            viewHolder.txtNombreUsuario = convertView.findViewById(R.id.txtNombreUsuario);
            viewHolder.txtCorreo = convertView.findViewById(R.id.txtCorreo);
            viewHolder.txtContrasena = convertView.findViewById(R.id.txtContraseña);


            // Establecer el ViewHolder como una etiqueta de la vista convertida
            convertView.setTag(viewHolder);
        } else {
            // Si convertView no es nulo, obtener el ViewHolder de la etiqueta
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Obtener el elemento de datos para la posición actual
        Usuario usuario = datos.get(position);

        // Establecer los datos en las vistas del diseño personalizado
        viewHolder.txtNombreUsuario.setText(usuario.getTextNombreUsuario());
        viewHolder.txtCorreo.setText(usuario.getTextCorreo());
        //viewHolder.txtContrasena.setText(usuario.getTextContrasena());

        return convertView;
    }

    private static class ViewHolder {
        TextView txtNombreUsuario;
        TextView txtCorreo;
        TextView txtContrasena;
    }
}