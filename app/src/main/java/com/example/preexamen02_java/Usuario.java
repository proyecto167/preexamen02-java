package com.example.preexamen02_java;

import java.io.Serializable;
import java.util.ArrayList;

public class Usuario implements Serializable {
    private int id;
    private String NombreUsuario;
    private String Correo;
    private String Contrasena;

    public Usuario() {

    }

    public Usuario(String nombreUsuario, String correo, String contrasena){
        this.NombreUsuario = nombreUsuario;
        this.Correo = correo;
        this.Contrasena =contrasena;
    }

    public int getId() { return id; }
    public void setId(int id) { this.id = id; }
    public String getTextNombreUsuario() { return NombreUsuario; }
    public void setTextNombreUsuario(String NombreUsuario) { this.NombreUsuario = NombreUsuario; }
    public String getTextCorreo() { return Correo; }
    public void setTextCorreo(String Correo) { this.Correo = Correo; }
    public String getTextContrasena(){ return Contrasena; }
    public void setTextContrasena(String Contrasena){
        this.Contrasena = Contrasena;
    }

    public static ArrayList<Usuario> llenarUsuarios(){
        ArrayList<Usuario> usuarios=new ArrayList<>();

        usuarios.add(new Usuario("Enzo","enzo0206@gmail.com","123"));
        usuarios.add(new Usuario("Juan","juan281@gmail.com","1234"));
        usuarios.add(new Usuario("Ernesto","ernesto777@gmail.com","12345"));

        return usuarios;
    }

}

