package com.example.preexamen02_java;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import Modelo.UsuariosDb;

public class registroActivity extends AppCompatActivity {
    private EditText editTextNombreUsuario;
    private EditText editTextCorreo;
    private EditText editTextContraseña;
    private EditText editTextContraseña2;
    private Button btnRegresar;
    private Button btnRegistrarse;
    private UsuariosDb usuariosDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registro_activity);

        editTextNombreUsuario = findViewById(R.id.txtNombreUsuario);
        editTextCorreo = findViewById(R.id.txtCorreo);
        editTextContraseña = findViewById(R.id.txtContraseña);
        editTextContraseña2 = findViewById(R.id.txtContraseña2);
        btnRegresar = findViewById(R.id.btnRegresar);
        btnRegistrarse = findViewById(R.id.btnRegistrarse);

        usuariosDb = new UsuariosDb(this);

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener los valores ingresados por el usuario
                String nombreUsuario = editTextNombreUsuario.getText().toString();
                String correo = editTextCorreo.getText().toString();
                String contraseña = editTextContraseña.getText().toString();
                String contraseña2 = editTextContraseña2.getText().toString();

                // Validar los campos ingresados
                if (validarCampos(nombreUsuario, correo, contraseña, contraseña2)) {

                    Toast.makeText(registroActivity.this, "Registro exitoso", Toast.LENGTH_SHORT).show();
                    usuariosDb.insertUsuario(new Usuario(nombreUsuario, correo, contraseña));

                    finish();
                }
            }
        });
    }

    private boolean validarCampos(String nombreUsuario, String correo, String contraseña, String contraseña2) {
        if (nombreUsuario.isEmpty() || correo.isEmpty() || contraseña.isEmpty() || contraseña2.isEmpty()) {
            Toast.makeText(registroActivity.this, "Llene todos los campos", Toast.LENGTH_SHORT).show();
            return false;
        } else if(!contraseña.equals(contraseña2)){
            Toast.makeText(registroActivity.this, "Las contraseñas no coinciden", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }
}