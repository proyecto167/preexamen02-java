package com.example.preexamen02_java;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import Modelo.UsuariosDb;

public class loginActivity extends AppCompatActivity {
    private EditText editTextCorreo;
    private EditText editTextContraseña;
    private Button btnIniciarSesion;
    private Button btnRegistrarse;
    private Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        // Obtener referencias a las vistas del layout
        editTextCorreo = findViewById(R.id.txtCorreo);
        editTextContraseña = findViewById(R.id.txtContraseña);
        btnIniciarSesion = findViewById(R.id.btnIngresar);

        btnRegistrarse = findViewById(R.id.btnRegistrarse);
        btnCerrar = findViewById(R.id.btnCerrar);

        btnIniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Obtener los valores ingresados por el usuario
                String correo = editTextCorreo.getText().toString();
                String contraseña = editTextContraseña.getText().toString();

                // Validar las credenciales ingresadas
                if (validarCredenciales(correo, contraseña)) {
                    iniciarMainActivity();
                } else {
                    Toast.makeText(loginActivity.this, "Datos inválidos", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Crear un Intent para iniciar la actividad RegistroActivity
                Intent intent = new Intent(loginActivity.this, registroActivity.class);
                startActivity(intent);
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
            }
        });
    }

    private boolean validarCredenciales(String correo, String contraseña) {
        UsuariosDb usuariosDb = new UsuariosDb(this);
        Usuario usuario = usuariosDb.getUsuario(correo);

        if (usuario != null && usuario.getTextContrasena().equals(contraseña)) {
            return true; // Credenciales válidas
        } else {
            return false; // Credenciales inválidas
        }
    }

    private void iniciarMainActivity() {
        Intent intent = new Intent(loginActivity.this, MainActivity.class);
        startActivity(intent);

        finish();
    }
}